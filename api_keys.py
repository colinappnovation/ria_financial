#!/usr/bin/env python3
import time
from settings import MANAGEMENT_API, SPACE_ID, client as CMA
from prettytable import PrettyTable as table

keys = CMA.api_keys(SPACE_ID).all()

o = table(['Key', 'Type', 'Created'])
o.align['Key'] = "l"

for k in keys:
    dt = k.sys.get('created_at')
    o.add_row([k.name, k.sys.get('type'), dt.strftime('%b %d %Y')])

print(o)