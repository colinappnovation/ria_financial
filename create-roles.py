#!/usr/bin/env python3
from contentful_management import Client
from contentful_management.errors import UnprocessableEntityError
from settings import MANAGEMENT_API, SPACE_ID

client = Client(MANAGEMENT_API)
name = "Content Editor - %s"
desc = "Can read content; Can write content in %s" 
locales = {'lt-LT': 'Lithuanian', 'pl-PL': 'Polish', 'es:ES': 'Spanish',
           'fr-CA': 'French (Canadian)'}
field = "fields.%.%s"

for code, lang in locales.items():
    f = field % code
    try:
      role = client.roles(SPACE_ID).create({
          'name': name % lang 
          'description': desc % lang,
          'permissions': {
              'ContentDelivery': [],
              'ContentModel': ['read'],
              'Settings': [],
              'Environments': []
          },
          'policies': [
            {'effect': 'allow', 'constraint': {'and': [
                        {'equals': [
                                {'doc': 'sys.type'
                                }, 'Entry'
                            ]
                        }
                    ]
                }, 'actions': ['read'
                ]
            },
            {'effect': 'allow', 'constraint': {'and': [
                        {'equals': [
                                {'doc': 'sys.type'
                                }, 'Entry'
                            ]
                        },
                        {'paths': [
                                {'doc': f'fields.%.{f}'
                                }
                            ]
                        }
                    ]
                }, 'actions': ['update'
                ]
            },
            {'effect': 'deny', 'constraint': {'and': [
                        {'equals': [
                                {'doc': 'sys.type'
                                }, 'Entry'
                            ]
                        },
                        {'equals': [
                                {'doc': 'sys.contentType.sys.id'
                                }, 'contentfulTools'
                            ]
                        }
                    ]
                }, 'actions': 'all'
            },
            {'effect': 'deny', 'constraint': {'and': [
                        {'equals': [
                                {'doc': 'sys.type'
                                }, 'Entry'
                            ]
                        },
                        {'equals': [
                                {'doc': 'sys.contentType.sys.id'
                                }, 'testPage'
                            ]
                        }
                    ]
                }, 'actions': 'all'
            },
            {'effect': 'allow', 'constraint': {'and': [
                        {'equals': [
                                {'doc': 'sys.type'
                                }, 'Asset'
                            ]
                        }
                    ]
                }, 'actions': ['read'
                ]
            },
            {'effect': 'allow', 'constraint': {'and': [
                        {'equals': [
                                {'doc': 'sys.type'
                                }, 'Asset'
                            ]
                        },
                        {'paths': [
                                {'doc': f'fields.%.{f}'
                                }
                            ]
                        }
                    ]
                }, 'actions': ['update'
                ]
            }
        ]
      })

      print(role.name + ' created with ID: ' + role.id +' successfully.')

    except UnprocessableEntityError as err:
      print(format(err))
