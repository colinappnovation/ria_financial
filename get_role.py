#!/usr/bin/env python3
import sys
from contentful_management import Client
from settings import MANAGEMENT_API, SPACE_ID

client = Client(MANAGEMENT_API)

role_id = sys.argv[1] if len(sys.argv) == 2 else "not supplied"

if role_id != "not supplied":
    role = client.roles(SPACE_ID).find(role_id)
    print(vars(role))
else:
    print("Please provide role id from Contentful UI.")
