#!/usr/bin/env python3
from settings import MANAGEMENT_API, SPACE_ID, ENV_ID, client
from colorama import Fore
from prettytable import PrettyTable as table

# Get all locales for space and environment combination
locales = client.locales(SPACE_ID, ENV_ID).all()

o = table(['Code', 'Name'])
o.align['Code'] = "l"
o.align['Name'] = "l"
 
# list compression syntax
[o.add_row([l.code, l.name]) for l in locales]

print(o)
print('============================================')
print(Fore.GREEN + f"No. of locales: {len(locales)}")