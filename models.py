#!/usr/bin/env python3
from settings import MANAGEMENT_API, SPACE_ID, ENV_ID, client
from prettytable import PrettyTable

# Get all locales for space and environment combination
types = client.content_types(SPACE_ID, ENV_ID).all()

o = PrettyTable(['Name', 'Version', 'Created', 'Updated'])
o.align['Name'] = "l"
o.align['Version'] = "l"

# list compression syntax
for t in types:
    dt = t.sys.get('created_at')
    up = t.sys.get('updated_at')
    o.add_row([t.name, t.sys.get('version'), dt.strftime('%b %d %Y'), up.strftime('%b %d %Y') ])

print(o)