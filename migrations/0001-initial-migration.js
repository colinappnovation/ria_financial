module.exports = function(migration) {
  const contentfulTools = migration
    .createContentType("contentfulTools")
    .name("ContentfulTools")
    .description("")
    .displayField("contentfulTools");
  contentfulTools
    .createField("contentfulTools")
    .name("ContentfulTools")
    .type("Symbol")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  contentfulTools.changeFieldControl(
    "contentfulTools",
    "extension",
    "contentful-tools",
    {}
  );
  const contentModelContainer = migration
    .createContentType("contentModelContainer")
    .name("ContentModelContainer")
    .description("A generic container for a collection of content models.")
    .displayField("id");

  contentModelContainer
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  contentModelContainer
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);

  contentModelContainer
    .createField("contentModels")
    .name("ContentModels")
    .type("Array")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",
      validations: [],
      linkType: "Entry"
    });

  contentModelContainer.changeFieldControl("id", "builtin", "singleLine", {});
  contentModelContainer.changeFieldControl("notes", "builtin", "markdown", {});

  contentModelContainer.changeFieldControl(
    "contentModels",
    "builtin",
    "entryCardsEditor",
    {
      bulkEditing: false
    }
  );

  const listOption = migration
    .createContentType("listOption")
    .name("ListOption")
    .description("A single list option.")
    .displayField("value");
  listOption
    .createField("value")
    .name("Value")
    .type("Symbol")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  listOption
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  listOption
    .createField("text")
    .name("Text")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  listOption.changeFieldControl("value", "builtin", "singleLine", {});
  listOption.changeFieldControl("notes", "builtin", "markdown", {});
  listOption.changeFieldControl("text", "builtin", "singleLine", {});
  const image = migration
    .createContentType("image")
    .name("Image")
    .description("A single image with alt-text.")
    .displayField("name");
  image
    .createField("name")
    .name("Name")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([])
    .disabled(false)
    .omitted(false);
  image
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  image
    .createField("altText")
    .name("AltText")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);

  image
    .createField("image")
    .name("Image")
    .type("Link")
    .localized(false)
    .required(true)
    .validations([
      {
        linkMimetypeGroup: ["attachment", "image", "video", "pdfdocument"]
      }
    ])
    .disabled(false)
    .omitted(false)
    .linkType("Asset");

  image.changeFieldControl("name", "builtin", "singleLine", {});
  image.changeFieldControl("notes", "builtin", "markdown", {});
  image.changeFieldControl("altText", "builtin", "singleLine", {});
  image.changeFieldControl("image", "builtin", "assetLinkEditor", {});
  const accordionSection = migration
    .createContentType("accordionSection")
    .name("AccordionSection")
    .description("A single section in an accordion.")
    .displayField("id");
  accordionSection
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  accordionSection
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  accordionSection
    .createField("label")
    .name("Label")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);

  accordionSection
    .createField("subSectionLabels")
    .name("SubSectionLabels")
    .type("Array")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Symbol",
      validations: []
    });

  accordionSection
    .createField("entries")
    .name("Entries")
    .type("Array")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",

      validations: [
        {
          linkContentType: ["accordionSectionEntry"]
        }
      ],

      linkType: "Entry"
    });

  accordionSection.changeFieldControl("id", "builtin", "singleLine", {});
  accordionSection.changeFieldControl("notes", "builtin", "markdown", {});
  accordionSection.changeFieldControl("label", "builtin", "singleLine", {});
  accordionSection.changeFieldControl(
    "subSectionLabels",
    "builtin",
    "listInput",
    {}
  );

  accordionSection.changeFieldControl(
    "entries",
    "builtin",
    "entryLinksEditor",
    {
      bulkEditing: false
    }
  );

  const optionsList = migration
    .createContentType("optionsList")
    .name("OptionsList")
    .description("A list of options")
    .displayField("id");

  optionsList
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(false)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  optionsList
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);

  optionsList
    .createField("options")
    .name("Options")
    .type("Array")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",

      validations: [
        {
          linkContentType: ["listOption"],
          message: "Entry must be of type ListOption"
        }
      ],

      linkType: "Entry"
    });

  optionsList.changeFieldControl("id", "builtin", "singleLine", {});
  optionsList.changeFieldControl("notes", "builtin", "markdown", {});

  optionsList.changeFieldControl("options", "builtin", "entryCardsEditor", {
    bulkEditing: false
  });

  const button = migration
    .createContentType("button")
    .name("Button")
    .description("A single button.")
    .displayField("id");

  button
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  button
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  button
    .createField("buttonText")
    .name("ButtonText")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  button
    .createField("buttonImage")
    .name("ButtonImage")
    .type("Link")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .linkType("Asset");
  button
    .createField("relativeHref")
    .name("RelativeHref")
    .type("Symbol")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  button.changeFieldControl("id", "builtin", "singleLine", {});
  button.changeFieldControl("notes", "builtin", "markdown", {});
  button.changeFieldControl("buttonText", "builtin", "singleLine", {});
  button.changeFieldControl("buttonImage", "builtin", "assetLinkEditor", {});
  button.changeFieldControl("relativeHref", "builtin", "singleLine", {});
  const accordionSectionEntry = migration
    .createContentType("accordionSectionEntry")
    .name("AccordionSectionEntry")
    .description("A single accordion subsection entry.")
    .displayField("id");

  accordionSectionEntry
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  accordionSectionEntry
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  accordionSectionEntry
    .createField("label")
    .name("Label")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  accordionSectionEntry
    .createField("body")
    .name("Body")
    .type("Text")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  accordionSectionEntry.changeFieldControl("id", "builtin", "singleLine", {});
  accordionSectionEntry.changeFieldControl("notes", "builtin", "markdown", {});
  accordionSectionEntry.changeFieldControl(
    "label",
    "builtin",
    "singleLine",
    {}
  );
  accordionSectionEntry.changeFieldControl("body", "builtin", "markdown", {});

  const accordion = migration
    .createContentType("accordion")
    .name("Accordion")
    .description(
      "An accordion with labels for each section. Does not contain inner content for each section."
    )
    .displayField("id");

  accordion
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  accordion
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  accordion
    .createField("title")
    .name("Title")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);

  accordion
    .createField("sections")
    .name("Sections")
    .type("Array")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",

      validations: [
        {
          linkContentType: ["accordionSection"],
          message: "Entry must be of type AccordionSection"
        }
      ],

      linkType: "Entry"
    });

  accordion.changeFieldControl("id", "builtin", "singleLine", {});
  accordion.changeFieldControl("notes", "builtin", "markdown", {});
  accordion.changeFieldControl("title", "builtin", "singleLine", {});

  accordion.changeFieldControl("sections", "builtin", "entryLinksEditor", {
    bulkEditing: false
  });

  const page = migration
    .createContentType("page")
    .name("Page")
    .description("Page wrapper")
    .displayField("id");

  page
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  page
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  page
    .createField("title")
    .name("Title")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);

  page
    .createField("contentModels")
    .name("ContentModels")
    .type("Array")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",
      validations: [],
      linkType: "Entry"
    });

  page
    .createField("keywords")
    .name("Keywords")
    .type("Text")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  page
    .createField("description")
    .name("Description")
    .type("Text")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  page.changeFieldControl("id", "builtin", "singleLine", {});
  page.changeFieldControl("notes", "builtin", "markdown", {});
  page.changeFieldControl("title", "builtin", "singleLine", {});

  page.changeFieldControl("contentModels", "builtin", "entryCardsEditor", {
    bulkEditing: false
  });

  page.changeFieldControl("keywords", "builtin", "markdown", {});
  page.changeFieldControl("description", "builtin", "markdown", {});
  const keyTranslations = migration
    .createContentType("keyTranslations")
    .name("KeyTranslations")
    .description("A single key value content pair.")
    .displayField("key");

  keyTranslations
    .createField("key")
    .name("Key")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  keyTranslations
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  keyTranslations
    .createField("value")
    .name("Value")
    .type("Text")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  keyTranslations.changeFieldControl("key", "builtin", "singleLine", {});
  keyTranslations.changeFieldControl("notes", "builtin", "markdown", {});
  keyTranslations.changeFieldControl("value", "builtin", "markdown", {});
  const testPage = migration
    .createContentType("testPage")
    .name("Test_Page")
    .description("Page wrapper")
    .displayField("id");

  testPage
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  testPage
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  testPage
    .createField("title")
    .name("Title")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);

  testPage
    .createField("contentModels")
    .name("ContentModels")
    .type("Array")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",
      validations: [],
      linkType: "Entry"
    });

  testPage
    .createField("keywords")
    .name("Keywords")
    .type("Text")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  testPage
    .createField("description")
    .name("Description")
    .type("Text")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  testPage.changeFieldControl("id", "builtin", "singleLine", {});
  testPage.changeFieldControl("notes", "builtin", "markdown", {});
  testPage.changeFieldControl("title", "builtin", "singleLine", {});

  testPage.changeFieldControl("contentModels", "builtin", "entryLinksEditor", {
    bulkEditing: false
  });

  testPage.changeFieldControl("keywords", "builtin", "markdown", {});
  testPage.changeFieldControl("description", "builtin", "markdown", {});
  const buttonLocalized = migration
    .createContentType("buttonLocalized")
    .name("ButtonLocalized")
    .description("A single button.")
    .displayField("id");

  buttonLocalized
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  buttonLocalized
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  buttonLocalized
    .createField("buttonText")
    .name("ButtonText")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  buttonLocalized
    .createField("buttonImage")
    .name("ButtonImage")
    .type("Link")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .linkType("Asset");
  buttonLocalized
    .createField("relativeHref")
    .name("RelativeHref")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  buttonLocalized.changeFieldControl("id", "builtin", "singleLine", {});
  buttonLocalized.changeFieldControl("notes", "builtin", "markdown", {});
  buttonLocalized.changeFieldControl("buttonText", "builtin", "singleLine", {});
  buttonLocalized.changeFieldControl(
    "buttonImage",
    "builtin",
    "assetLinkEditor",
    {}
  );
  buttonLocalized.changeFieldControl(
    "relativeHref",
    "builtin",
    "singleLine",
    {}
  );
  const form = migration
    .createContentType("form")
    .name("Form")
    .description("A form.")
    .displayField("id");

  form
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  form
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  form
    .createField("title")
    .name("Title")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);

  form
    .createField("subtitles")
    .name("Subtitles")
    .type("Array")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Symbol",
      validations: []
    });

  form
    .createField("formFields")
    .name("FormFields")
    .type("Array")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",

      validations: [
        {
          linkContentType: ["formField"],
          message: "Entry must be of content type FormField"
        }
      ],

      linkType: "Entry"
    });

  form
    .createField("button")
    .name("Button")
    .type("Link")
    .localized(false)
    .required(false)
    .validations([
      {
        linkContentType: ["button"]
      }
    ])
    .disabled(false)
    .omitted(false)
    .linkType("Entry");

  form
    .createField("additionalContent")
    .name("AdditionalContent")
    .type("Array")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",
      validations: [],
      linkType: "Entry"
    });

  form.changeFieldControl("id", "builtin", "singleLine", {});
  form.changeFieldControl("notes", "builtin", "markdown", {});
  form.changeFieldControl("title", "builtin", "singleLine", {});
  form.changeFieldControl("subtitles", "builtin", "listInput", {});

  form.changeFieldControl("formFields", "builtin", "entryCardsEditor", {
    bulkEditing: false
  });

  form.changeFieldControl("button", "builtin", "entryCardEditor", {});

  form.changeFieldControl("additionalContent", "builtin", "entryCardsEditor", {
    bulkEditing: false
  });

  const text = migration
    .createContentType("text")
    .name("Text")
    .description("A single text entry.")
    .displayField("id");

  text
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  text
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  text
    .createField("text")
    .name("Text")
    .type("Text")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  text.changeFieldControl("id", "builtin", "singleLine", {});
  text.changeFieldControl("notes", "builtin", "markdown", {});
  text.changeFieldControl("text", "builtin", "markdown", {});
  const formField = migration
    .createContentType("formField")
    .name("FormField")
    .description("A single form field.")
    .displayField("id");

  formField
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  formField
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  formField
    .createField("label")
    .name("Label")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  formField
    .createField("additionalText")
    .name("AdditionalText")
    .type("Text")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);

  formField
    .createField("optionsList")
    .name("OptionsList")
    .type("Link")
    .localized(false)
    .required(false)
    .validations([
      {
        linkContentType: ["optionsList", "optionsListLocalized"]
      }
    ])
    .disabled(false)
    .omitted(false)
    .linkType("Entry");

  formField
    .createField("placeholder")
    .name("Placeholder")
    .type("Symbol")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);

  formField
    .createField("validators")
    .name("Validators")
    .type("Array")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",

      validations: [
        {
          linkContentType: ["validator"],
          message: "Entry must be of type: Validator"
        }
      ],

      linkType: "Entry"
    });

  formField.changeFieldControl("id", "builtin", "singleLine", {});
  formField.changeFieldControl("notes", "builtin", "markdown", {});
  formField.changeFieldControl("label", "builtin", "singleLine", {});
  formField.changeFieldControl("additionalText", "builtin", "markdown", {});
  formField.changeFieldControl("optionsList", "builtin", "entryLinkEditor", {});
  formField.changeFieldControl("placeholder", "builtin", "singleLine", {});
  formField.changeFieldControl("validators", "builtin", "entryLinksEditor", {});

  const optionsListLocalized = migration
    .createContentType("optionsListLocalized")
    .name("OptionsListLocalized")
    .description(
      "An options list type with localizable list options. This options list has a different list of options per locale."
    )
    .displayField("id");

  optionsListLocalized
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  optionsListLocalized
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);

  optionsListLocalized
    .createField("options")
    .name("Options")
    .type("Array")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",

      validations: [
        {
          linkContentType: ["listOption"]
        }
      ],

      linkType: "Entry"
    });

  optionsListLocalized
    .createField("localeCopier")
    .name("LocaleCopier")
    .type("Symbol")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  optionsListLocalized.changeFieldControl("id", "builtin", "singleLine", {});
  optionsListLocalized.changeFieldControl("notes", "builtin", "markdown", {});

  optionsListLocalized.changeFieldControl(
    "options",
    "builtin",
    "entryLinksEditor",
    {
      bulkEditing: false
    }
  );

  optionsListLocalized.changeFieldControl(
    "localeCopier",
    "extension",
    "locale-copier",
    {}
  );
  const validator = migration
    .createContentType("validator")
    .name("Validator")
    .description("Defines a validation message for a single field validator.")
    .displayField("id");
  validator
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([])
    .disabled(false)
    .omitted(false);
  validator
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  validator
    .createField("message")
    .name("Message")
    .type("Text")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false);
  validator.changeFieldControl("id", "builtin", "singleLine", {});
  validator.changeFieldControl("notes", "builtin", "markdown", {});
  validator.changeFieldControl("message", "builtin", "markdown", {});
  const contentModelContainerLocalized = migration
    .createContentType("contentModelContainerLocalized")
    .name("ContentModelContainerLocalized")
    .description("A generic container for a collection of content models.")
    .displayField("id");

  contentModelContainerLocalized
    .createField("id")
    .name("Id")
    .type("Symbol")
    .localized(false)
    .required(true)
    .validations([
      {
        unique: true
      }
    ])
    .disabled(false)
    .omitted(false);

  contentModelContainerLocalized
    .createField("notes")
    .name("Notes")
    .type("Text")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);

  contentModelContainerLocalized
    .createField("contentModels")
    .name("ContentModels")
    .type("Array")
    .localized(true)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(false)
    .items({
      type: "Link",
      validations: [],
      linkType: "Entry"
    });

  contentModelContainerLocalized
    .createField("localeCopier")
    .name("LocaleCopier")
    .type("Symbol")
    .localized(false)
    .required(false)
    .validations([])
    .disabled(false)
    .omitted(true);
  contentModelContainerLocalized.changeFieldControl(
    "id",
    "builtin",
    "singleLine",
    {}
  );
  contentModelContainerLocalized.changeFieldControl(
    "notes",
    "builtin",
    "markdown",
    {}
  );

  contentModelContainerLocalized.changeFieldControl(
    "contentModels",
    "builtin",
    "entryCardsEditor",
    {
      bulkEditing: false
    }
  );

  contentModelContainerLocalized.changeFieldControl(
    "localeCopier",
    "extension",
    "locale-copier",
    {}
  );
};
