#!/usr/bin/env python3
from settings import MANAGEMENT_API, SPACE_ID, ENV_ID, client
 
# Get all locales for space and environment combination
extensions = client.ui_extensions(SPACE_ID, ENV_ID).all()
 
# list compression syntax
[print(e.name) for e in extensions]