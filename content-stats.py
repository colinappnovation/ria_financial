#!/usr/bin/env python3
from settings import SPACE_ID, DELIVERY_ID, ENV_ID, client as cma
from contentful import Client as cda
import time
from prettytable import PrettyTable as table
from colorama import Fore

# locales = cma.locales(SPACE_ID, ENV_ID).all()

client = cda(
  SPACE_ID,
  DELIVERY_ID,
  environment=ENV_ID
)

# for l in locales:
#     localized_entries = client.entries({'locale': l.code})
#     print(f"{l.code} has {localized_entries.total} records")
#     time.sleep(1)

types = cma.content_types(SPACE_ID, ENV_ID).all()
all_records = 0

o = table(['Content Type', 'Entries'])
o.align['Content Type'] = "l"
o.align['Entries'] = "l"

def getTotal(id):
    type_entries = client.entries({'content_type': id})
    print(Fore.LIGHTGREEN_EX + f'Processed: {id}')
    return type_entries.total

for t in types:    
    total = getTotal(t.id)
    o.add_row([t.name, total])
    all_records += total
    time.sleep(1)

print(o)
print(f"TOTAL RECORDS: {all_records}")