#!/usr/bin/env python3
from settings import MANAGEMENT_API, SPACE_ID, ENV_ID, client
from prettytable import PrettyTable as table

# Get all locales for space and environment combination
environments = client.environments(SPACE_ID).all()

o = table(['Name', 'Version', 'Created'])
o.align['Name'] = "l"

# environment status
for env in environments:
    environment = client.environments(SPACE_ID).find(env.name)
    #print(f'Name: {env.name} Version: {environment.sys.get("version")} Created: {environment.sys.get("created_at")}')
    o.add_row([env.name, environment.sys.get("version"), environment.sys.get("created_at")])

print(o)