#!/usr/bin/env python3
import os
from dotenv import load_dotenv, find_dotenv
from contentful_management import Client

# init colorama
from colorama import init
init()

# load .env file keys
load_dotenv(find_dotenv())

# Grab Environment settings
MANAGEMENT_API = os.getenv("MANAGEMENT_API")
SPACE_ID = os.getenv("SPACE_ID")
ENV_ID = os.getenv("ENV_ID")
DELIVERY_ID = os.getenv("DELIVERY_ID")

# Establish client
client = Client(MANAGEMENT_API)
