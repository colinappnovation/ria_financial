# Ria Financial Contentful Engagement

Contentful assistance for Ria Financial

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Python 3
* PIP
* (Contentful Management Python Client Package)[https://github.com/contentful/contentful-management.py]
* Contentment Management Access Token
* Dotenv python package
* Node (NPM or Yarn)

### Installing

1. pip install -r requirements.txt
2. Create a .env file with keys

```
MANAGEMENT_API=
SPACE_ID=
ENV_ID=
```

## Authors

* **Colin McClure** - *Initial work*

## How to Execute

```
python locales.py
python roles.py
python models.py
```
