#!/usr/bin/env python3
from prettytable import PrettyTable as table
from settings import SPACE_ID, client

# Get all locales for space and environment combination
ROLES = client.roles(SPACE_ID).all()

o = table(['Role', 'ID', 'Created'])
o.align['Role'] = "l"

# list compression syntax
[o.add_row([r.name, r.id, r.sys.get('created_at')]) for r in ROLES]

print(o)
