#!/usr/bin/env python3
from colorama import Fore
from settings import SPACE_ID, client as CMA
from prettytable import PrettyTable as table

memberships = CMA.memberships(SPACE_ID).all()

o = table(['Is Admin', 'UID'])
o.align['Is Admin'] = "l"

admins = 0
for m in memberships:
    uid = m.user.get("sys").get("id")
    if m.admin: admins += 1
    o.add_row([m.admin, uid])

print(o)
print('================================================================================')
print(Fore.RED + f"TOTAL ADMINS: {admins} ({round(admins / len(memberships)  * 100)}%)")
print(Fore.GREEN + f"TOTAL MEMBERS: {len(memberships)}")
